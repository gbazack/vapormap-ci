# In this file is defined a Gitlab CI/CD pipeline for the GeoMapping application.

# Default configuration
default:
  image: python:3.8

# Environment variables
variables:
  FRONTEND_IMAGE_TAG: $CI_REGISTRY_IMAGE/frontend:latest
  BACKEND_IMAGE_TAG: $CI_REGISTRY_IMAGE/backend:latest
  VAPORMAP_BACKEND: localhost
  VAPORMAP_BACKEND_PORT: 8001

# LIST OF STAGES
stages:
  - security
  - codequality
  - build
  - test
  - deploy

# STAGE: SECURITY
# Static application security testing
.sast:
  stage: security
  script:
    - echo "Static Application Security Testing"
#include:
#  - template: Security/SAST.gitlab-ci.yml

# Check the installed dependencies for known security vulnerabilities, with Safety
saftety-check:
  stage: security
  script:
    - echo "Check installed dependencies for known vulnerabilities"
    - python -m pip install safety
    - safety check

# STAGE: CODEQUALITY
# Static analyze of python code by flake8
flake8-analyzer:
  stage: codequality
  script:
    - echo "Static testing with Python Flake8 package"
    - python -m pip install flake8
    - mkdir reports
    - flake8 --max-line-length 120 --max-complexity 10 app >> reports/flake8-report.txt
  except:
    - master
  allow_failure: true
  artifacts:
    untracked: false
    expire_in: 1 day
    paths:
      - reports/
  when: manual


# Test the docker files with hadolint
docker-linter:
  image: hadolint/hadolint:v2.0.0-alpine
  stage: codequality
  script:
    - echo "Lint the dockerfile and docker-compose files"
    - cd app
    - hadolint -f gitlab_codeclimate Dockerfile >> ../reports/backend-hadolint.json
    - cd ..
    - cd frontend
    - hadolint -f gitlab_codeclimate Dockerfile >> ../reports/frontend-hadolint.json
  except:
    - master 
  allow_failure: true
  artifacts:
    untracked: false
    expire_in: 1 day
    reports:
      codequality:
        - "reports/*"
    paths:
      - "reports/*"
  when: manual


# STAGE: BUILD
build-frontend:
  image: docker:20.10
  services:
    - docker:20.10-dind
  stage: build
  only:
    - master
  before_script:
    - docker info
  script:
    - echo "Building docker image for the frontend"
    - cd frontend
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --no-cache --build-arg $VAPORMAP_BACKEND --build-arg VAPORMAP_BACKEND_PORT -t $FRONTEND_IMAGE_TAG .
    - docker push $FRONTEND_IMAGE_TAG
  when: manual

build-backend:
  image: docker:20.10
  services:
    - docker:20.10-dind
  stage: build
  only:
    - master 
  before_script:
    - docker info
  script:
    - echo "Building docker image for the backend"
    - cd app
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build --no-cache -t $BACKEND_IMAGE_TAG .
    - docker push $BACKEND_IMAGE_TAG
  when: manual

# STAGE: TEST
test-backend-pytest:
  stage: test
  script:
    - python -m pip install pytest
    - touch report.xml
    - pytest --junitxml=report.xml
  artifacts:
    untracked: false
    reports:
      junit: report.xml
    paths:
      - report.xml
  only:
    - master